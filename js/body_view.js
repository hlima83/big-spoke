var BodyView = Backbone.View.extend({
	el: 'body',
	dragging: false,
	drag_element: null,
	model: null,
	events: {
		"mousedown .component" : "dragComponent",
		"mousemove" : "moveComponent",
		"mouseup" : "dropComponent"	
	},
	dragComponent: function(e){
			
			var compClasses = e.target.className.split(" ");
			//component inside palette
			if(compClasses.indexOf('comp-inside-palette') != -1){
				var newCompView = this.duplicateComponent(e.target);
				$(newCompView.el).css({top: e.target.offsetTop, left: e.target.offsetLeft});
				$('#main-area').append(newCompView.render().el);
				this.drag_element = newCompView;
				this.dragging = true;
			}
	},
	moveComponent: function(e){
		if(this.dragging){		
			$(this.drag_element.el).css({left: e.pageX, top: e.pageY});
		}
	},
	dropComponent: function(e){
		if(this.dragging){
			this.dragging = false;
			//$(this.drag_element.el).css({zIndex: 1});
			this.addComponent(this.drag_element.model);	
		}
	},
	duplicateComponent: function(elem){
		var newComp = new Component({compType: elem.firstChild.title, name: elem.firstChild.title, connections: new ConnectionsList}); 
		var newCompView = new ComponentView({model: newComp, className: 'component comp-outside-palette'});
		return newCompView;
	},
	addComponent: function(model){
		componentsArea.add(model);
	}
});
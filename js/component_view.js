var connecting=false;
var conn_elem1_model=null;
var conn_elem1_target=null;
var conn_elem2_model=null;
var conn_elem2_target=null;

var ComponentView = Backbone.View.extend({
	tagName: 'div',
	render: function(){	
		$(this.el).append('<input readonly type="text" class="comp-label" title="' + this.model.get('compType') + '" value="' + this.model.get('name') + '" align="center" class="comp-label"></input>');
		return this;
	},
	events: {
		"click" : "connectComponent",
		"click .comp-label" : "changeLabel",
		"change .comp-label" : "saveLabel",
	},
	changeLabel: function(e){
		var compClasses = e.target.parentNode.className.split(" ");
		
		if(compClasses.indexOf('comp-outside-palette') != -1){
			e.target.style.zIndex = 1;
			e.target.style.backgroundColor = '#FFFF99';
			e.target.readOnly = false;
		}
	},
	saveLabel: function(e){
		this.model.set({name: e.target.value});
		e.target.style.backgroundColor = "#FFFFFF";
		e.target.readOnly = true;
	},
	
	connectComponent: function(e){
		var compClasses = e.target.className.split(" ");
		if(compClasses.indexOf('comp-outside-palette') != -1){
			if(!connecting){
				conn_elem1_model = this.model;
				conn_elem1_target = e.target;
				connecting = true;
			}
			else{
				conn_elem2_model = this.model;
				conn_elem2_target = e.target;
				connecting = false;
				var connName=prompt("Connection name:");
				makeConnection(conn_elem1_model, conn_elem2_model, connName);
				drawConnection(conn_elem1_target, conn_elem2_target, connName);
			}
		}
	}
});

function makeConnection(elem1, elem2, connName){
	elem1.get('connections').add(new Connection({elem: elem2.get('name'), dir: 'to', name: connName}));
	elem2.get('connections').add(new Connection({elem: elem1.get('name'), dir: 'from', name: connName}));
}

function drawConnection(e1,e2,connName){
	if(document.getElementById('connections-canvas') == null){
		var connCanvas = document.createElement('canvas');
		connCanvas.id = 'connections-canvas';
		connCanvas.width = $('#main-area').width();
		connCanvas.height = $('#main-area').height();		
	}
	else{
		var connCanvas = document.getElementById('connections-canvas');
	}
	if(connCanvas.getContext){
		var ctx = connCanvas.getContext('2d');
		var fromX = e1.offsetLeft - $('#sidebar').width() - 8 + (e2.offsetLeft > e1.offsetLeft ? $(e1).width() : 0);
		var fromY = e1.offsetTop - 8 + ($(e1).height()/2);
		var toX = e2.offsetLeft - $('#sidebar').width() - 8 + (e2.offsetLeft > e1.offsetLeft ? 0 : $(e2).width());
		var toY = e2.offsetTop - 8 + ($(e2).height()/2);
		var angle = Math.atan2(toY-fromY,toX-fromX);
		ctx.beginPath();
		ctx.moveTo(fromX, fromY);
		ctx.lineTo(toX, toY);
		ctx.lineTo(toX-10*Math.cos(angle-Math.PI/6),toY-10*Math.sin(angle-Math.PI/6));
		ctx.moveTo(toX, toY);
		ctx.lineTo(toX-10*Math.cos(angle+Math.PI/6),toY-10*Math.sin(angle+Math.PI/6));
		ctx.closePath();
		ctx.stroke();
	}
	var connSubPar = document.createElement('p');
	var connSubLab = document.createTextNode(connName);
	connSubPar.appendChild(connSubLab);
	connSubPar.style.position = 'absolute';
	connSubPar.style.fontSize = '12px';
	connSubPar.style.top = (e1.offsetTop > e2.offsetTop ? e1.offsetTop : e1.offsetTop + $(e1).width()/2) + 'px';
	connSubPar.style.left = e1.offsetLeft + (e2.offsetLeft > e1.offsetLeft ? $(e1).width() : -$(e1).width()/2) + 8 + 'px';
	
	document.getElementById('main-area').insertBefore(connCanvas, document.getElementById('main-area').firstChild);
	$('#main-area').append(connSubPar);
}
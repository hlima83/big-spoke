var GenerateView = Backbone.View.extend({
	events: {
		"click" : "generateXML",
	},
	generateXML: function(){
		//construct xml
		var dom = jsxml.fromString('<?xml version="1.0" encoding="UTF-8"?><root/>');
		var cjProcElem = dom.createElement('cj_process');
		var componentsElem = dom.createElement('components');
		cjProcElem.appendChild(componentsElem);
		
		componentsArea.each(function(comp){
				var compElem = dom.createElement('component');
				compElem.setAttribute('label',comp.get('name'));
				compElem.setAttribute('class',comp.get('compType'));
				
				//connections
				var connections = comp.get('connections');
				if(connections.length > 0){
					var transitionsElem = dom.createElement('transitions');
					
					connections.each(function(conn){
						var transitionElem = dom.createElement(conn.get('dir'));
						transitionElem.setAttribute('name', conn.get('elem'));
						transitionElem.setAttribute('label', conn.get('name'));
						transitionsElem.appendChild(transitionElem);
					});
					
					compElem.appendChild(transitionsElem);
				}
				
				
				componentsElem.appendChild(compElem);
				
		});
		
		dom.documentElement.appendChild(cjProcElem);
		var xmlBlob = new Blob([jsxml.toXml(dom)], { type: 'application/xml' });
		var blobUrl = window.URL.createObjectURL(xmlBlob);
		window.open(blobUrl, '_blank');
		
		//alert(componentsArea.at(1).get('connections').length);
	}
});
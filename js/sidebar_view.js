var SidebarView = Backbone.View.extend({
	render: function(components){
		var parentEl = this.el;
		components.each(function(comp){
			var view = new ComponentView({model: comp, className: 'component comp-inside-palette'});
			parentEl.appendChild(view.render().el);
		});
		this.el = parentEl;
	},
});
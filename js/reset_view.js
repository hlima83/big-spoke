var ResetView = Backbone.View.extend({
	events: {
		"click" : "cleanArea",
	},
	cleanArea: function(){
		var conf = confirm('Do you really want to reset the workflow?');
		if(conf){
			$('#main-area').html('');
			componentsArea = new ComponentsList;
		}
	}
});
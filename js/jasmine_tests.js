describe("Palette rendering", function() {
	var sidebar;
	var componentsPal;
	
	beforeEach(function() {
		var testPal = document.createElement('div');
		testPal.id = 'test-palette';
		testPal.style.display = 'none';
		document.getElementsByTagName('body')[0].appendChild(testPal);
		sidebar = new SidebarView({el: $('#test-palette')});
		componentsPal = new ComponentsList([{compType: 'C1', name: 'C1'},{compType: 'C2', name: 'C2'},{compType: 'C3', name: 'C3'}]);
		sidebar.render(componentsPal);
	});	
		
	it("Components rendered in palette", function() {
		expect(sidebar.el.childNodes.length).toEqual(3);
	});
});

describe("Palette events", function() {
	var body;
	
	beforeEach(function() {
		body = new BodyView;
		componentsArea = new ComponentsList;
	});	
		
	it("Component duplication", function() {
		var testComp = document.createElement('div');
		var testLabel = document.createElement('input');
		testLabel.title = 'C1';
		testLabel.value = 'C1';
		testComp.appendChild(testLabel);
		var dupView = body.duplicateComponent(testComp);
		expect(dupView.model.get('compType')).toEqual('C1');
		expect(dupView.model.get('name')).toEqual('C1');
	});
	
	it("Component creation", function() {
		body.addComponent(new Component);
		expect(componentsArea.length).toEqual(1);
	});	
});

describe("Main area events", function() {
	it("Component connection", function() {
		var testView1 = new ComponentView({model: new Component({compType: 'C1', name: 'comp1', connections: new ConnectionsList})});
		var testView2 = new ComponentView({model: new Component({compType: 'C2', name: 'comp2', connections: new ConnectionsList})});
		makeConnection(testView1.model, testView2.model, 'testConnection');
		expect(testView1.model.get('connections').length).toEqual(1);
		expect(testView2.model.get('connections').length).toEqual(1);
		expect(testView1.model.get('connections').at(0).get('dir')).toEqual('to');
		expect(testView1.model.get('connections').at(0).get('elem')).toEqual(testView2.model.get('name'));
		expect(testView2.model.get('connections').at(0).get('dir')).toEqual('from');
		expect(testView2.model.get('connections').at(0).get('elem')).toEqual(testView1.model.get('name'));
	});
});



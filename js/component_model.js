var Connection = Backbone.Model.extend({
	defaults: { elem: null, dir: null, name: null }
});

var ConnectionsList = Backbone.Collection.extend({
	model: Connection
});

var Component = Backbone.Model.extend({
	defaults: { compType: 'c1', name: 'c1', connections: new ConnectionsList },
});

var ComponentsList = Backbone.Collection.extend({
	model: Component
});

var componentsArea = new ComponentsList;
var componentsPal = new ComponentsList([{compType: 'C1', name: 'C1'},{compType: 'C2', name: 'C2'}, {compType: 'C3', name: 'C3'}, {compType: 'C4', name: 'C4'}, {compType: 'C5', name: 'C5'}]);
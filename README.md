# Clubjudge Assignment


## Overview

The point of this test assignment is to develop a visual workflow definition editor. There are 2 parts to the assignment: fulfilling the assignment's requirements and a "questions&answers" segment after delivery. There are primary and secondary requirements - try to focus on the primary ones first.

### Desired functionality

Mockup of the app to develop:

![](https://dl.dropbox.com/u/540375/qzp1.png)

Project BigSpoke is all about process workflow definitions. There are 2 main areas of interaction: a sidebar and a main interaction area. On the top of the sidebar, there is a components palette (C1,C2,C3, etc.), a GENERATE button and a RESET button. These components are draggable to the main interaction area, like this image shows:

![](https://dl.dropbox.com/u/540375/qzp_drag.png)

Once a component is dragged onto the main interaction area, it's name (where it shows "C1") will have input focus, so that the user can fill in a name for the component dragged. Once more than one component is dragged in, a user can also form links between the components, and these link have names as well. Example:

![](https://dl.dropbox.com/u/540375/qzp_2.png)

In this case, the user has chosen to link NAME to OTHER NAME, and has given LINK NAME as the name for the link. How links are inputted by the user is up to you, but it should be close to a simple drag-and-drop action as well.

Once several components are placed, a user can press the GENERATE button to generate an XML dump of what's on the main interaction area. For example, for a process workflow that looks like this:

![](https://dl.dropbox.com/u/540375/qzp_3.png)

And assuming INPUT1 and INPUT2 are C1 components, SUCCESS is a C2 component and ERROR is a C3 component, then when the user would press GENERATE, the output would trigger a file download prompt, and the file would contain:

```xml
<cj_process>
	<components>
		<component label="INPUT1" class="C1">
		  <transitions>
		  	<to name="SUCCESS" label="on_success"/>
		  	<to name="INPUT2" label="on_help"/>
		  </transitions>
		</component>
		<component label="INPUT2" class="C1">
		  <transitions>
		  	<to name="SUCCESS" label="on_option1"/>
		  	<to name="ERROR" label="on_option2"/>
		  	<from name="INPUT1" label="on_help"/>
		  </transitions>
		</component>
		<component label="SUCCESS" class="C2">
		  <transitions>
		  	<from name="INPUT1" label="on_success"/>
		  	<from name="INPUT2" label="on_option_1"/>
		  </transitions>
		</component>
		<component label="ERROR" class="C3">
		  <transitions>
		    <from name="INPUT2" label="on_option_2"/>
		</component>
	</components>
</cj_process>
```

After pressing RESET, the main interaction area is wiped. There should be a confirmation prompt. The components (C1,C2, etc.) should be easy to add to the app, i.e. not hardcoded.


### Requirements

* some sort of javascript frontend framework used (primary)
* some measure of code coverage with a testing framework of your choice (primary)
* Backbone.js as the framework choice (secondary)
* not using jQuery (secondary)
* a cool feature that we didn't ask for (secondary)

### Deadline

The deadline is set to July 12th. If you need more time (2/3 days), let us know. If you have any questions, let us know.

Good luck!
